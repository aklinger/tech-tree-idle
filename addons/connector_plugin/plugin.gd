tool
extends EditorPlugin

var myNode: BaseTreeNode

func edit(object: Object) -> void:
	print("editing %s" % object.get_path())
	myNode = object

func make_visible(visible: bool) -> void:
	if not myNode:
		return
	if not visible:
		myNode = null
	update_overlays()

func handles(object: Object) -> bool:
	# Required to use forward_canvas_draw_... below
	return object is BaseTreeNode

func forward_canvas_draw_over_viewport(overlay: Control) -> void:
	if not myNode or not myNode.is_inside_tree():
		return


func convert_to_local_coordinates(event_position: Vector2, node: BaseTreeNode) -> Vector2:
	# Calculate the position of the mouse cursor relative to the RectExtents' center
	var viewport_transform_inv := node.get_viewport().get_global_canvas_transform().affine_inverse()
	var viewport_position: Vector2 = viewport_transform_inv.xform(event_position)
	var transform_inv := node.get_global_transform().affine_inverse()
	var target_position: Vector2 = transform_inv.xform(viewport_position.round())
	return target_position

func search(node: Node, position: Vector2) -> Node:
	for child in node.get_children():
		if child is BaseTreeNode:
			var myChild = child as BaseTreeNode
			var localPos = convert_to_local_coordinates(position, myChild)
			var centerPoint = myChild.get_global_rect().position + myChild.get_global_rect().size / 2
			var dist = centerPoint.distance_to(localPos)
			if localPos.length() < 100:
				return myChild
		else:
			var found = search(child, position)
			if found != null:
				return found
	return null

func forward_canvas_gui_input(event: InputEvent) -> bool:
	if not myNode or not myNode.visible:
		return false
	
	if event is InputEventMouseButton:
		var mouseEvent = event as InputEventMouseButton
		if mouseEvent.pressed and mouseEvent.button_index == BUTTON_RIGHT:
			var edit_root = get_tree().get_edited_scene_root()
			var searched = search(edit_root, mouseEvent.position)
			if searched != null and searched != myNode:
				
				var oldDependsOn = myNode.dependsOn
				myNode.dependsOn = myNode.get_path_to(searched)
				
				var undo := get_undo_redo()
				undo.create_action("Change Node Dependency")

				undo.add_do_property(myNode, "dependsOn", myNode.dependsOn)
				undo.add_undo_property(myNode, "dependsOn", oldDependsOn)

				undo.commit_action()
				
				myNode.update()
	return false
