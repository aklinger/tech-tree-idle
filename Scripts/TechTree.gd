extends Control

var friends = 1
var friends_occupied = 0
var cookies = 0
var money = 0
var respect = 0
var diamonds = 0

var oven_level = 1

func updateHUD():
	if friends > 0:
		$HUD/WorkerLabel.text  = "Friends:  " + str(friends) + " (" + str(friends - friends_occupied) + ")"
	else:
		$HUD/WorkerLabel.text  = ""
	if cookies > 0:
		$HUD/CookieLabel.text  = "Cookies:  " + str(cookies)
	else:
		$HUD/CookieLabel.text  = ""
	if money > 0:
		$HUD/MoneyLabel.text   = "Money:    " + str(money)
	else:
		$HUD/MoneyLabel.text  = ""
	if respect > 0:
		$HUD/RespectLabel.text = "Respect:  " + str(respect)
	else:
		$HUD/RespectLabel.text  = ""
	if diamonds > 0:
		$HUD/DiamondLabel.text = "Diamonds: " + str(diamonds)
	else:
		$HUD/DiamondLabel.text  = ""

func payCosts(friendCost, respectCheck, cookieCost, moneyCost, diamondsCost):
	if friends - friends_occupied < friendCost:
		return false
	if respect < respectCheck:
		return false
	if cookies < cookieCost:
		return false
	if money < moneyCost:
		return false
	if diamonds < diamondsCost:
		return false
	friends_occupied += friendCost
	cookies -= cookieCost
	money -= moneyCost
	diamonds -= diamondsCost
	return true

func reAllocFriends(frends):
	if friends - friends_occupied < frends:
		return false
	friends_occupied += frends
	return true

func freeFriends(freends):
	friends_occupied -= freends

func _process(_delta):
	updateHUD()

func _on_Bake_Cookies_finished(_count):
	cookies += 10 * oven_level

func _on_Better_Oven_finished(_count):
	oven_level += 1

func _on_Sell_Cookies_finished(_count):
	money += 10

func _on_Make_Friends_finished(count):
	friends += 1

func _on_Gift_Cookies_finished(count):
	respect += 1
	friends += 1


func _on_World_Peace2_finished(count):
	respect += 1000
	friends += 1
