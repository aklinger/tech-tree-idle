tool
extends VBoxContainer
class_name BaseTreeNode

export var title : String
export var desc : String
export var time : float = 1
export var researchCost : int = 1
export var cookieCost : int
export var moneyCost : int
export var respectCheck : int
export var diamondCost : int

export var oneshot : bool = true

export var dependsOn : NodePath

var count = 0
var running : bool

signal started()
signal finished(count)

var tree

func _ready():
	tree = get_parent()

func _process(_delta):
	$Button.text = title
	
	if not dependsOn:
		visible = true
	else:
		var dependency = get_node(dependsOn)
		if dependency.count > 0:
			visible = true
		else:
			if not Engine.editor_hint:
				visible = false
	var text = ""
	if oneshot:
		text += "Upgrade\n"
	else:
		text += "Action\n"
	
	if cookieCost > 0 or moneyCost > 0 or diamondCost > 0 or time > 0:
		text += "\nCost: \n"
	if time > 0:
		text += str(time) + " Time Unit"+S(time)+"\n"
	if cookieCost > 0:
		text += str(cookieCost) + " Cookie"+S(cookieCost)+"\n"
	if moneyCost > 0:
		text += str(moneyCost) + " Money"+S(moneyCost)+"\n"
	if diamondCost > 0:
		text += str(diamondCost) + " Diamond"+S(diamondCost)+"\n"
	
	if respectCheck > 0 or researchCost > 0:
		text += "\nNeeds: \n"
	if respectCheck > 0:
		text += str(respectCheck) + " Respect\n"
	if researchCost > 0:
		text += str(researchCost) + " Friend"+S(researchCost)+"\n"
	
	$Button.hint_tooltip = text + "\n" + desc
	if $Timer.is_stopped():
		$ProgressBar.ratio = 0
		$ProgressBar.percent_visible = false
	else:
		$ProgressBar.ratio = ($Timer.wait_time - $Timer.time_left) / $Timer.wait_time
		$ProgressBar.percent_visible = true

func S(number):
	if number == 1:
		return ""
	else:
		return "s"

func _on_Button_pressed():
	if !running and (not oneshot or count <= 0):
		if tree.payCosts(researchCost, respectCheck, cookieCost, moneyCost, diamondCost):
			$Timer.start(time)
			running = true
			emit_signal("started")
	else:
		if not $Timer.paused:
			$Timer.paused = true
			tree.freeFriends(researchCost)
		else:
			if tree.reAllocFriends(researchCost):
				$Timer.paused = false

func _on_Timer_timeout():
	running = false
	tree.freeFriends(researchCost)
	count += 1
	if oneshot:
		$Button.disabled = true
	emit_signal("finished", count)

func _draw():
	if Engine.editor_hint and dependsOn:
		var dependency = get_node(dependsOn)
		var color = Color(1, 1, 0, 0.5)
		draw_line(rect_size / 2, dependency.rect_position + dependency.rect_size / 2 - rect_position, color, 2)

